library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity compress is
    port ( h_in         : in  unsigned((32*8 - 1) downto 0);
           chunk        : in  unsigned((64*8 - 1) downto 0);
           counter      : in  unsigned((64 - 1) downto 0);
           last_chunk   : in  std_logic;
           start        : in  std_logic;
           reset        : in  std_logic;
           clk          : in  std_logic;
           h_out        : out unsigned((32*8 - 1) downto 0);
           ready        : out std_logic := '0'
           );
end compress;

architecture Behavioral of compress is

 component mix is
     port ( a_in  : in  unsigned((32 - 1) downto 0);
             b_in  : in  unsigned((32 - 1) downto 0);
             c_in  : in  unsigned((32 - 1) downto 0);
             d_in  : in  unsigned((32 - 1) downto 0);
             x     : in  unsigned((32 - 1) downto 0);
             y     : in  unsigned((32 - 1) downto 0);
           --  clk   : in std_logic;
             a_out : out unsigned((32 - 1) downto 0);
             b_out : out unsigned((32 - 1) downto 0);
             c_out : out unsigned((32 - 1) downto 0);
             d_out : out unsigned((32 - 1) downto 0)
             );
  end component;
  
    type matrix2D is array (integer range <> , integer range <> )  of integer range 0 to 15;
    type type_state is (
		STATE_IDLE,
		STATE_INIT,
		STATE_IMP_1,
		STATE_IMP_2,
		STATE_IMP_3,
        STATE_IMP_4,
		STATE_HASH, 
		STATE_RESET
		
	);
		                                                                                                    -- fixed initialize Vectors
	constant IV0               : unsigned((32 - 1) downto 0) := x"6A09E667"; --F3BCC908";   --Frac(Sqrt(2))               
    constant IV1               : unsigned((32 - 1) downto 0) := x"BB67AE85"; --84CAA73B";   --Frac(Sqrt(3))
    constant IV2               : unsigned((32 - 1) downto 0) := x"3C6EF372"; --FE94F82B";   --Frac(Sqrt(5))
    constant IV3               : unsigned((32 - 1) downto 0) := x"A54FF53A"; --5F1D36F1";   --Frac(Sqrt(7))
    constant IV4               : unsigned((32 - 1) downto 0) := x"510E527F"; --ADE682D1";   --Frac(Sqrt(11))
    constant IV5               : unsigned((32 - 1) downto 0) := x"9B05688C"; --2B3E6C1F";   --Frac(Sqrt(13))
    constant IV6               : unsigned((32 - 1) downto 0) := x"1F83D9AB"; --FB41BD6B";   --Frac(Sqrt(17))
    constant IV7               : unsigned((32 - 1) downto 0) := x"5BE0CD19"; --137E2179";   --Frac(Sqrt(19))
   
    constant SIGMA             : matrix2D(0 to 9, 0 to 15) := (                                             -- SIGMA Matrix 
            0 => (0 => 00, 1 => 01, 2 => 02, 3 => 03, 4 => 04, 5 => 05, 6 => 06, 7 => 07, 8 => 08, 9 => 09, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15),
            1 => (0 => 14, 1 => 10, 2 => 04, 3 => 08, 4 => 09, 5 => 15, 6 => 13, 7 => 06, 8 => 01, 9 => 12, 10 => 00, 11 => 02, 12 => 11, 13 => 07, 14 => 05, 15 => 03), 
            2 => (0 => 11, 1 => 08, 2 => 12, 3 => 00, 4 => 05, 5 => 02, 6 => 15, 7 => 13, 8 => 10, 9 => 14, 10 => 03, 11 => 06, 12 => 07, 13 => 01, 14 => 09, 15 => 04), 
            3 => (0 => 07, 1 => 09, 2 => 03, 3 => 01, 4 => 13, 5 => 12, 6 => 11, 7 => 14, 8 => 02, 9 => 06, 10 => 05, 11 => 10, 12 => 04, 13 => 00, 14 => 15, 15 => 08), 
            4 => (0 => 09, 1 => 00, 2 => 05, 3 => 07, 4 => 02, 5 => 04, 6 => 10, 7 => 15, 8 => 14, 9 => 01, 10 => 11, 11 => 12, 12 => 06, 13 => 08, 14 => 03, 15 => 13), 
            5 => (0 => 02, 1 => 12, 2 => 06, 3 => 10, 4 => 00, 5 => 11, 6 => 08, 7 => 03, 8 => 04, 9 => 13, 10 => 07, 11 => 05, 12 => 15, 13 => 14, 14 => 01, 15 => 09), 
            6 => (0 => 12, 1 => 05, 2 => 01, 3 => 15, 4 => 14, 5 => 13, 6 => 04, 7 => 10, 8 => 00, 9 => 07, 10 => 06, 11 => 03, 12 => 09, 13 => 02, 14 => 08, 15 => 11), 
            7 => (0 => 13, 1 => 11, 2 => 07, 3 => 14, 4 => 12, 5 => 01, 6 => 03, 7 => 09, 8 => 05, 9 => 00, 10 => 15, 11 => 04, 12 => 08, 13 => 06, 14 => 02, 15 => 10), 
            8 => (0 => 06, 1 => 15, 2 => 14, 3 => 09, 4 => 11, 5 => 03, 6 => 00, 7 => 08, 8 => 12, 9 => 02, 10 => 13, 11 => 07, 12 => 01, 13 => 04, 14 => 10, 15 => 05), 
            9 => (0 => 10, 1 => 02, 2 => 08, 3 => 04, 4 => 07, 5 => 06, 6 => 01, 7 => 05, 8 => 15, 9 => 11, 10 => 09, 11 => 14, 12 => 03, 13 => 12, 14 => 13, 15 => 00));
            
                                    
                                    
    signal state                : type_state := STATE_RESET; 
    signal chunk_saved          : unsigned((64*8 - 1) downto 0);
    signal last_chunk_saved     : std_logic;
    signal start_old            : std_logic := '0';     -- flag detection start
    signal V                    : unsigned((64*8 - 1) downto 0) := (others => '0');
    signal test                 : integer range 0 to 15; 
    signal test3                : unsigned(15 downto 0);
 
    --- signals for the four mixers
    signal mix1_a_in,  mix1_b_in,  mix1_c_in,  mix1_d_in, mix1_x, mix1_y : unsigned((32 - 1) downto 0);
    signal mix1_a_out, mix1_b_out, mix1_c_out, mix1_d_out : unsigned((32 - 1) downto 0);
    
    signal mix2_a_in,  mix2_b_in,  mix2_c_in,  mix2_d_in, mix2_x, mix2_y : unsigned((32 - 1) downto 0);
    signal mix2_a_out, mix2_b_out, mix2_c_out, mix2_d_out : unsigned((32 - 1) downto 0);
    
    signal mix3_a_in,  mix3_b_in,  mix3_c_in,  mix3_d_in, mix3_x, mix3_y : unsigned((32 - 1) downto 0);
    signal mix3_a_out, mix3_b_out, mix3_c_out, mix3_d_out : unsigned((32 - 1) downto 0);
    
    signal mix4_a_in,  mix4_b_in,  mix4_c_in,  mix4_d_in, mix4_x, mix4_y : unsigned((32 - 1) downto 0);
    signal mix4_a_out, mix4_b_out, mix4_c_out, mix4_d_out : unsigned((32 - 1) downto 0);
   
    
begin

    mix1: mix port map (
                    a_in  => mix1_a_in, 
                    b_in  => mix1_b_in,
                    c_in  => mix1_c_in,
                    d_in  => mix1_d_in, 
                    x     => mix1_x,
                    y     => mix1_y,
                    a_out => mix1_a_out, 
                    b_out => mix1_b_out,
                    c_out => mix1_c_out,
                    d_out => mix1_d_out);
    mix2: mix port map (
                   a_in  => mix2_a_in, 
                   b_in  => mix2_b_in,
                   c_in  => mix2_c_in,
                   d_in  => mix2_d_in, 
                   x     => mix2_x,
                   y     => mix2_y,
                   a_out => mix2_a_out, 
                   b_out => mix2_b_out,
                   c_out => mix2_c_out,
                   d_out => mix2_d_out);
    mix3: mix port map (
                    a_in  => mix3_a_in, 
                    b_in  => mix3_b_in,
                    c_in  => mix3_c_in,
                    d_in  => mix3_d_in, 
                    x     => mix3_x,
                    y     => mix3_y,
                    a_out => mix3_a_out, 
                    b_out => mix3_b_out,
                    c_out => mix3_c_out,
                    d_out => mix3_d_out);                                                                                               
    mix4: mix port map (
                  a_in  => mix4_a_in, 
                  b_in  => mix4_b_in,
                  c_in  => mix4_c_in,
                  d_in  => mix4_d_in, 
                  x     => mix4_x,
                  y     => mix4_y,
                  a_out => mix4_a_out, 
                  b_out => mix4_b_out,
                  c_out => mix4_c_out,
                  d_out => mix4_d_out);
                                                                                  
    process(clk, reset)
    variable iteration : integer range 0 to 16 := 0; 
    variable h_buffer  : unsigned((32*8 - 1) downto 0);
    variable test2                :  unsigned (15 downto 0) := x"000A";
    begin 
      if reset = '1' then           -- asynchron reset
        state <= STATE_IDLE;
        start_old <= '0';
       -- h_out <= (others => '0');
        
      end if;
      if rising_edge(clk) then   
         test2 := test2 ror 2;
         test3 <= test2;
        case state is               -- state machine 
        
            when STATE_RESET =>
            
                h_out       <= (others => '0');
                ready       <= '0';
                if reset = '0' then 
                    state   <= STATE_IDLE;
                    ready   <= '0';
                end if;
                    
                
            when STATE_IDLE  =>                             -- idle (fertig)
                if (start = '1' and start_old = '0') then 
                     ready               <= '0';
                     state               <= STATE_INIT;
                     h_buffer            := h_in;
                     chunk_saved         <= chunk;
                     last_chunk_saved    <= last_chunk;
                else               
                     ready               <= '1';
                end if;
                
                    
            when STATE_INIT  =>                             -- initialize local work vector V  
                V(32*8 -1 downto 0)     <= h_buffer;                -- V(0-7)
                V(32*9 -1 downto 32*8)  <= IV0;                 
                V(32*10-1 downto 32*9)  <= IV1;
                V(32*11-1 downto 32*10) <= IV2;
                V(32*12-1 downto 32*11) <= IV3;
                V(32*13-1 downto 32*12) <= IV4 xor counter(32-1 downto 0);     -- V12
                V(32*14-1 downto 32*13) <= IV5 xor counter(64-1 downto 32);    -- V13
                if (last_chunk = '1') then 
                    V(32*15-1 downto 32*14) <= IV6 xor x"FFFFFFFF";            -- V14
                else 
                    V(32*15-1 downto 32*14) <= IV6;                            -- V14
                end if;
                V(32*16-1 downto 32*15) <= IV7;                                -- V15
                
                ready       <= '0';
                h_out       <= (others => '0');          
                state       <= STATE_IMP_1;
                iteration   := 0; 
                
            when STATE_IMP_1  =>                         -- first half of first mixing 
            
                mix1_a_in  <= V(32*1 -1 downto 32*0 );          -- 0
                mix1_b_in  <= V(32*5 -1 downto 32*4 );          -- 4
                mix1_c_in  <= V(32*9 -1 downto 32*8 );          -- 8
                mix1_d_in  <= V(32*13-1 downto 32*12);          -- 12
                mix1_x     <= chunk_saved((SIGMA(iteration, 0)+1)*32 -1 downto (SIGMA(iteration, 0)*32));
                mix1_y     <= chunk_saved((SIGMA(iteration, 1)+1)*32 -1 downto (SIGMA(iteration, 1)*32));
                 --               test <= SIGMA(iteration, 0);
                mix2_a_in  <= V(32*2 -1 downto 32*1 );          -- 1
                mix2_b_in  <= V(32*6 -1 downto 32*5 );          -- 5
                mix2_c_in  <= V(32*10-1 downto 32*9 );          -- 9
                mix2_d_in  <= V(32*14-1 downto 32*13);          -- 13
                mix2_x     <= chunk_saved((SIGMA(iteration, 2)+1)*32 -1 downto (SIGMA(iteration, 2)*32));
                mix2_y     <= chunk_saved((SIGMA(iteration, 3)+1)*32 -1 downto (SIGMA(iteration, 3)*32));                
                
                mix3_a_in  <= V(32*3 -1 downto 32*2 );          -- 2
                mix3_b_in  <= V(32*7 -1 downto 32*6 );          -- 6
                mix3_c_in  <= V(32*11-1 downto 32*10);          -- 10
                mix3_d_in  <= V(32*15-1 downto 32*14);          -- 14
                mix3_x     <= chunk_saved((SIGMA(iteration, 4)+1)*32 -1 downto (SIGMA(iteration, 4)*32));
                mix3_y     <= chunk_saved((SIGMA(iteration, 5)+1)*32 -1 downto (SIGMA(iteration, 5)*32));                
                
                mix4_a_in  <= V(32*4 -1 downto 32*3 );          -- 3
                mix4_b_in  <= V(32*8 -1 downto 32*7 );          -- 7
                mix4_c_in  <= V(32*12-1 downto 32*11);          -- 11
                mix4_d_in  <= V(32*16-1 downto 32*15);          -- 15
                mix4_x     <= chunk_saved((SIGMA(iteration, 6)+1)*32 -1 downto (SIGMA(iteration, 6)*32));
                mix4_y     <= chunk_saved((SIGMA(iteration, 7)+1)*32 -1 downto (SIGMA(iteration, 7)*32));                
                                
                state <= STATE_IMP_2; 
                    
            when STATE_IMP_2  =>                         -- second half of first mixing
            
                V(32*1 -1 downto 32*0 ) <= mix1_a_out;          -- 0
                V(32*5 -1 downto 32*4 ) <= mix1_b_out;          -- 4
                V(32*9 -1 downto 32*8 ) <= mix1_c_out;          -- 8
                V(32*13-1 downto 32*12) <= mix1_d_out;          -- 12

                V(32*2 -1 downto 32*1 ) <= mix2_a_out;          -- 1
                V(32*6 -1 downto 32*5 ) <= mix2_b_out;          -- 5
                V(32*10-1 downto 32*9 ) <= mix2_c_out;          -- 9
                V(32*14-1 downto 32*13) <= mix2_d_out;          -- 13

                V(32*3 -1 downto 32*2 ) <= mix3_a_out;          -- 2
                V(32*7 -1 downto 32*6 ) <= mix3_b_out;          -- 6
                V(32*11-1 downto 32*10) <= mix3_c_out;          -- 10
                V(32*15-1 downto 32*14) <= mix3_d_out;          -- 14
                
                V(32*4 -1 downto 32*3 ) <= mix4_a_out;          -- 3
                V(32*8 -1 downto 32*7 ) <= mix4_b_out;          -- 7
                V(32*12-1 downto 32*11) <= mix4_c_out;          -- 11
                V(32*16-1 downto 32*15) <= mix4_d_out;          -- 15
                
                state <= STATE_IMP_3;
                
        when STATE_IMP_3  =>                         -- first half of second mixing 
            
                mix1_a_in  <= V(32*1 -1 downto 32*0 );          -- 0
                mix1_b_in  <= V(32*6 -1 downto 32*5 );          -- 5
                mix1_c_in  <= V(32*11-1 downto 32*10);          -- 10
                mix1_d_in  <= V(32*16-1 downto 32*15);          -- 15
                mix1_x     <= chunk_saved((SIGMA(iteration, 8)+1)*32 -1 downto (SIGMA(iteration, 8)*32));
                mix1_y     <= chunk_saved((SIGMA(iteration, 9)+1)*32 -1 downto (SIGMA(iteration, 9)*32));
                                
                mix2_a_in  <= V(32*2 -1 downto 32*1 );          -- 1
                mix2_b_in  <= V(32*7 -1 downto 32*6 );          -- 6
                mix2_c_in  <= V(32*12-1 downto 32*11);          -- 11
                mix2_d_in  <= V(32*13-1 downto 32*12);          -- 12
                mix2_x     <= chunk_saved((SIGMA(iteration,10)+1)*32 -1 downto (SIGMA(iteration,10)*32));
                mix2_y     <= chunk_saved((SIGMA(iteration,11)+1)*32 -1 downto (SIGMA(iteration,11)*32));                
                
                mix3_a_in  <= V(32*3 -1 downto 32*2 );          -- 2
                mix3_b_in  <= V(32*8 -1 downto 32*7 );          -- 7
                mix3_c_in  <= V(32*9 -1 downto 32*8 );          -- 8
                mix3_d_in  <= V(32*14-1 downto 32*13);          -- 13
                mix3_x     <= chunk_saved((SIGMA(iteration,12)+1)*32 -1 downto (SIGMA(iteration,12)*32));
                mix3_y     <= chunk_saved((SIGMA(iteration,13)+1)*32 -1 downto (SIGMA(iteration,13)*32));                
                
                mix4_a_in  <= V(32*4 -1 downto 32*3 );          -- 3    
                mix4_b_in  <= V(32*5 -1 downto 32*4 );          -- 4
                mix4_c_in  <= V(32*10-1 downto 32*9 );          -- 9
                mix4_d_in  <= V(32*15-1 downto 32*14);          -- 14
                mix4_x     <= chunk_saved((SIGMA(iteration,14)+1)*32 -1 downto (SIGMA(iteration,14)*32));
                mix4_y     <= chunk_saved((SIGMA(iteration,15)+1)*32 -1 downto (SIGMA(iteration,15)*32));                
                                
                state <= STATE_IMP_4; 
                    
            when STATE_IMP_4  =>                         -- second half of second mixing
            
                V(32*1 -1 downto 32*0 ) <= mix1_a_out;          -- 0
                V(32*6 -1 downto 32*5 ) <= mix1_b_out;          -- 5
                V(32*11-1 downto 32*10) <= mix1_c_out;          -- 10
                V(32*16-1 downto 32*15) <= mix1_d_out;          -- 15
    
                V(32*2 -1 downto 32*1 ) <= mix2_a_out;          -- 1
                V(32*7 -1 downto 32*6 ) <= mix2_b_out;          -- 6
                V(32*12-1 downto 32*11) <= mix2_c_out;          -- 11
                V(32*13-1 downto 32*12) <= mix2_d_out;          -- 12
    
                V(32*3 -1 downto 32*2 ) <= mix3_a_out;          -- 2
                V(32*8 -1 downto 32*7 ) <= mix3_b_out;          -- 7
                V(32*9 -1 downto 32*8 ) <= mix3_c_out;          -- 8
                V(32*14-1 downto 32*13) <= mix3_d_out;          -- 13
                
                V(32*4 -1 downto 32*3 ) <= mix4_a_out;          -- 3
                V(32*5 -1 downto 32*4 ) <= mix4_b_out;          -- 4
                V(32*10-1 downto 32*9 ) <= mix4_c_out;          -- 9
                V(32*15-1 downto 32*14) <= mix4_d_out;          -- 14
                
                
                  
                iteration := iteration +1;
                if iteration = 10 then 
                      state <= STATE_HASH;
                else 
                      state <= STATE_IMP_1;
                end if;
                        
            when STATE_HASH  => 
                
               -- h_buffer := h_buffer  xor V(32*8 -1 downto 32*0);   -- h(0-7) xor V(0-7)
               --  h_out <= h_buffer xor V(32*16-1 downto 32*8);
              h_out <= h_buffer xor V(32*8 -1 downto 32*0) xor V(32*16-1 downto 32*8);
               ready <= '1';
                state <= STATE_IDLE;
            
             
            when others =>
                state <= STATE_IDLE;
                   
        end case;
        
                
        start_old <= start;
      end if;
      
   end process;
    		
end Behavioral;