entity blake2s is
    generic (
        --the algorithm could handle messages up to a length of 2**128 bytes,
		--but vhdl cannot handle numbers that big. The maximal length required
		--by argon is 1032, therefore that value was chosen as a limit.
        MAX_MESSAGE_LEN_BYTE 	: integer := 1032;

        -- maximum hash length 
        MAX_HASH_LEN_BYTE      	: integer := 64
      );
	port (
		--high active reset signal
		reset		: in	std_logic;
		
		--system clock
		clk		: in	std_logic;

		--chunk of message to be hashed
		message		: in	std_logic_vector(MAX_MESSAGE_LEN_BYTE*8-1 downto 0);

		--desired hash lenght in bytes
		hash_len	: in	integer range 1 to MAX_HASH_LEN_BYTE;

		--high as long as chunks are sent
		valid_in	: in	std_logic;

		--number of bytes to be hashed

		message_len	: in	integer range 0 to MAX_MESSAGE_LEN_BYTE;

		--high when the last chunk is sent
		last_chunk	: in	std_logic;

		--ready for next chunk
		compress_ready	: out	std_logic;

		--high when the output is valid
		valid_out	: out	std_logic;

		--the generated hash in little endian
		hash		:out	std_logic_vector(MAX_HASH_LEN_BYTE*8-1 downto 0)
	);
end blake2s;
