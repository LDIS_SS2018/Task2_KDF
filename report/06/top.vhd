-------------------------------------------------------------------------------
--
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-------------------------------------------------------------------------------
--
entity team6 is
    generic (
        DEG_OF_PARALLELISM  : integer := 4;
        MAX_TAG_LEN_BYTE    : integer := 1024;
        MAX_ITERATION_COUNT : integer := 1e6
    );
    port (
        reset                   : in std_logic;
        clk                     : in std_logic;
        start                   : in std_logic;
        
        argon_type              : in integer range 0 to 2;
        tag_len_byte            : in integer range 1 to MAX_TAG_LEN_BYTE;
        block_rows_count        : in integer;
        block_columns_count     : in integer;
        iteration_count         : in integer range 0 to MAX_ITERATION_COUNT;
        mem_r_w                 : in std_logic; 
        mem_ready               : in std_logic;
        mem_data_out            : in std_logic_vector(7 downto 0);

        mem_address             : out std_logic_vector(26 downto 0); 
        mem_data_in             : out std_logic_vector(7 downto 0); 
        current_iteration_count : out integer range 0 to MAX_ITERATION_COUNT;
        tag                     : out std_logic_vector(MAX_TAG_LEN_BYTE * 8 - 1 downto 0);
        ready                   : out std_logic
    );
end team6;
--
-------------------------------------------------------------------------------
--
architecture behavioral of team6 is
    signal counter : integer   :=  0;
    signal m_start : std_logic := '0';
    signal s_current_iteration_count : integer range 0 to MAX_ITERATION_COUNT;
    
begin
    process (clk, reset)
    begin
        if(reset = '1') then
            counter                   <=  0;
            ready                     <= '0';
            s_current_iteration_count <=  0;
        elsif(clk'event and clk='1') then  
            ready <= '1';
            if(start = '1' or m_start = '1') then
                if(counter < 2) then
                    ready                     <= '0';
                    m_start                   <= '1';
                    counter                   <= counter + 1;
                    s_current_iteration_count <= s_current_iteration_count + 1;
                else 
                    ready                     <= '1';
                    m_start                   <= '0';
                    counter                   <=  0;
                    s_current_iteration_count <=  0;
                    tag                       <=  (others => '1');
                end if;
            end if;
        end if;
    end process;

    current_iteration_count <= s_current_iteration_count;
end behavioral;
