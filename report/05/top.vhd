-------------------------------------------------------------------------------
--
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-------------------------------------------------------------------------------
--
entity team5 is
    generic (
        DEG_OF_PARALLELISM    : integer := 4;
        MAX_PASSWORD_LEN_BYTE : integer := 256;
        MAX_SALT_LEN_BYTE     : integer := 16;
        MAX_TAG_LEN_BYTE      : integer := 1024
    );
    port (
        reset                   : in std_logic;
        clk                     : in std_logic;
        start                   : in std_logic;
        
        tag_len_byte            : in integer range 1 to MAX_TAG_LEN_BYTE;
        password                : in std_logic_vector(MAX_PASSWORD_LEN_BYTE * 8 - 1 downto 0);
        password_len_byte       : in integer range 0 to MAX_PASSWORD_LEN_BYTE;
        salt                    : in std_logic_vector(MAX_SALT_LEN_BYTE * 8 - 1 downto 0);
        salt_len_byte           : in integer range 0 to MAX_SALT_LEN_BYTE;
        mem_r_w                 : in std_logic; 
        mem_ready               : in std_logic;
        mem_data_out            : in std_logic_vector(7 downto 0);

        mem_address         : out std_logic_vector(26 downto 0); 
        mem_data_in         : out std_logic_vector(7 downto 0); 
        block_rows_count    : out integer;
        block_columns_count : out integer;
        ready               : out std_logic
    );
end team5;
--
-------------------------------------------------------------------------------
--
architecture behavioral of team5 is
    signal counter : integer   :=  0;
    signal m_start : std_logic := '0';

begin
    process (clk, reset)
    begin
        if(reset = '1') then
            counter <= 0;
            ready   <= '0';
        elsif(clk'event and clk='1') then  
            ready <= '1';
            if(start = '1' or m_start = '1') then
                if(counter < 2) then
                    ready   <= '0';
                    m_start <= '1';
                    counter <= counter + 1;
                else 
                    ready               <= '1';
                    m_start             <= '0';
                    counter             <=  0;
                    block_rows_count    <=  4;
                    block_columns_count <=  4;
                end if;
            end if;
        end if;
    end process;
end behavioral;
