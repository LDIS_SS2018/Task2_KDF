-------------------------------------------------------------------------------
-- Application for Argon2
-- Andreas Hirtenlehner
-- Valentin Hanser
--
-- May 2018
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-- -----------------------------------------------------------------------------
--
entity Argon2 is
    generic (
        -- Degree of paralellism -- set by SPECIFICATION MAIL to 4
        DEG_OF_PARALLELISM    : integer := 4;

        -- maximum tag length -- set by SPECIFICATION MAIL to 1024bytes
        MAX_TAG_LEN_BYTE      : integer := 1024;

        -- maximum password length -- set by SPECIFICATION MAIL to 256bytes
        MAX_PASSWORD_LEN_BYTE : integer := 256;

        -- maximum salt length -- recommended by PAPER: 16bytes
        MAX_SALT_LEN_BYTE     : integer := 16;

        -- maximum iteratio count -- limited by SPECIFICATION
        MAX_ITERATION_COUNT   : integer := 1e6
      );
    port (
		clk        : in std_logic;
        clk_200mhz : in std_logic;
        reset      : in std_logic;
        start      : in std_logic;

        password          : in std_logic_vector(MAX_PASSWORD_LEN_BYTE * 8 - 1 downto 0);
        password_len_byte : in integer range 0 to MAX_PASSWORD_LEN_BYTE;

        iteration_count : in integer range 0 to MAX_ITERATION_COUNT := 10;

        -- length of tag in bytes -- range Specified in PAPER
        tag_len_byte : in integer range 1 to MAX_TAG_LEN_BYTE := 64;

        -- Type y of Argon2: 0 for Argon2d, 1 for Argon2i, 2 for Argon2id
        -- default value=1 set by SPECIFICATION   
        argon_type : in integer range 0 to 2 := 1;

        -- Nonce S salt for password hashing 
        -- PAPER: length 16byte is recommended 
        salt          : in std_logic_vector(MAX_SALT_LEN_BYTE * 8 - 1 downto 0);
        salt_len_byte : in integer range 0 to MAX_SALT_LEN_BYTE := 16;

        -- The Argon2 output is a string tag_len_byte bytes long 
        -- size limited by SPECIFICATION MAIL 
        tag : out std_logic_vector(MAX_TAG_LEN_BYTE * 8 - 1 downto 0);

        ready : out std_logic;
        current_iteration_count : out integer range 0 to MAX_ITERATION_COUNT := 0;

        -- DDR2 interface
        ddr2_addr            : out   std_logic_vector(12 downto 0);
        ddr2_ba              : out   std_logic_vector(2 downto 0);
        ddr2_ras_n           : out   std_logic;
        ddr2_cas_n           : out   std_logic;
        ddr2_we_n            : out   std_logic;
        ddr2_ck_p            : out   std_logic_vector(0 downto 0);
        ddr2_ck_n            : out   std_logic_vector(0 downto 0);
        ddr2_cke             : out   std_logic_vector(0 downto 0);
        ddr2_cs_n            : out   std_logic_vector(0 downto 0);
        ddr2_dm              : out   std_logic_vector(1 downto 0);
        ddr2_odt             : out   std_logic_vector(0 downto 0);
        ddr2_dq              : inout std_logic_vector(15 downto 0);
        ddr2_dqs_p           : inout std_logic_vector(1 downto 0);
        ddr2_dqs_n           : inout std_logic_vector(1 downto 0)
	);
end Argon2;
--
-------------------------------------------------------------------------------
--
architecture behavioral of Argon2 is
--
-------------------------------------------------------------------------------
--  Signals
-------------------------------------------------------------------------------
--
    signal s_ready      : std_logic := '0';
    signal s_ready_next : std_logic := '0';

    -- input buffer
    signal s_password_buf          : std_logic_vector(MAX_PASSWORD_LEN_BYTE * 8 - 1 downto 0);
    signal s_password_len_byte_buf : integer range 0 to MAX_PASSWORD_LEN_BYTE;
    signal s_iteration_count_buf   : integer range 0 to MAX_ITERATION_COUNT := 10;
    signal s_tag_len_byte_buf      : integer range 0 to MAX_TAG_LEN_BYTE := 64;
    signal s_argon_type_buf        : integer range 0 to 2 := 1;
    signal s_salt_buf              : std_logic_vector(MAX_SALT_LEN_BYTE * 8 - 1 downto 0);
    signal s_salt_len_byte_buf     : integer range 0 to MAX_SALT_LEN_BYTE := 16;

    signal s_team5_start          : std_logic := '0';
    signal s_team5_start_next     : std_logic := '0';
    signal s_team5_ready          : std_logic := '0';
    signal s_team5_ready_pos_edge : std_logic := '0';
    signal s_team5_ready_ff       : std_logic_vector(1 downto 0)  := (others => '0');

    signal s_team6_start          : std_logic := '0';
    signal s_team6_start_next     : std_logic := '0';
    signal s_team6_ready          : std_logic := '0';
    signal s_team6_ready_pos_edge : std_logic := '0';
    signal s_team6_ready_ff       : std_logic_vector(1 downto 0)  := (others => '0');

    signal s_block_rows_count    : integer;
    signal s_block_columns_count : integer;   

    signal s_mem_address  : std_logic_vector(26 downto 0); -- address space
    signal s_mem_data_in  : std_logic_vector(7 downto 0); -- data byte input
    signal s_mem_r_w      : std_logic; -- Read or Write flag: '1' ... write, '0' ... read
    signal s_mem_ready    : std_logic; -- allocated memory ready or busy flag: '1' ... ready, '0' ... busy
    signal s_mem_data_out : std_logic_vector(7 downto 0);

    signal s_mem_address_team5  : std_logic_vector(26 downto 0); -- address space
    signal s_mem_data_in_team5  : std_logic_vector(7 downto 0); -- data byte input
    signal s_mem_r_w_team5      : std_logic; -- Read or Write flag: '1' ... write, '0' ... read
    signal s_mem_ready_team5    : std_logic; -- allocated memory ready or busy flag: '1' ... ready, '0' ... busy
    signal s_mem_data_out_team5 : std_logic_vector(7 downto 0);

    signal s_mem_address_team6  : std_logic_vector(26 downto 0); -- address space
    signal s_mem_data_in_team6  : std_logic_vector(7 downto 0); -- data byte input
    signal s_mem_r_w_team6      : std_logic; -- Read or Write flag: '1' ... write, '0' ... read
    signal s_mem_ready_team6    : std_logic; -- allocated memory ready or busy flag: '1' ... ready, '0' ... busy
    signal s_mem_data_out_team6 : std_logic_vector(7 downto 0);

    type state_t is (
		STATE_IDLE,
        STATE_TEAM5,
		STATE_TEAM6
	);

    signal state      : state_t := STATE_IDLE;
    signal state_next : state_t := STATE_IDLE;
--
-------------------------------------------------------------------------------
--  Component declarations
-------------------------------------------------------------------------------
--
    component team5 is
        generic (
            DEG_OF_PARALLELISM      : integer;
            MAX_PASSWORD_LEN_BYTE   : integer;
            MAX_SALT_LEN_BYTE       : integer;
            MAX_TAG_LEN_BYTE        : integer
        );
        port (
            reset                   : in std_logic;
            clk                     : in std_logic;
            start                   : in std_logic;
            tag_len_byte            : in integer range 1 to MAX_TAG_LEN_BYTE;
            password                : in std_logic_vector(MAX_PASSWORD_LEN_BYTE * 8 - 1 downto 0);
            password_len_byte       : in integer range 0 to MAX_PASSWORD_LEN_BYTE;
            salt                    : in std_logic_vector(MAX_SALT_LEN_BYTE * 8 - 1 downto 0);
            salt_len_byte           : in integer range 0 to MAX_SALT_LEN_BYTE;
            mem_r_w                 : in std_logic; 
            mem_ready               : in std_logic;
            mem_data_out            : in std_logic_vector(7 downto 0);

            mem_address         : out std_logic_vector(26 downto 0); 
            mem_data_in         : out std_logic_vector(7 downto 0); 
            block_rows_count    : out integer;
            block_columns_count : out integer;
            ready               : out std_logic
        );
    end component;

    component team6 is
        generic (
            DEG_OF_PARALLELISM  : integer;
            MAX_TAG_LEN_BYTE    : integer;
            MAX_ITERATION_COUNT : integer
        );
        port (
            reset                   : in std_logic;
            clk                     : in std_logic;
            start                   : in std_logic;
            argon_type              : in integer range 0 to 2;
            tag_len_byte            : in integer range 1 to MAX_TAG_LEN_BYTE;
            block_rows_count        : in integer;
            block_columns_count     : in integer;
            iteration_count         : in integer range 0 to MAX_ITERATION_COUNT;
            mem_r_w                 : in std_logic; 
            mem_ready               : in std_logic;
            mem_data_out            : in std_logic_vector(7 downto 0);

            mem_address             : out std_logic_vector(26 downto 0); 
            mem_data_in             : out std_logic_vector(7 downto 0); 
            current_iteration_count : out integer range 0 to MAX_ITERATION_COUNT;
            tag                     : out std_logic_vector(MAX_TAG_LEN_BYTE * 8 - 1 downto 0);
            ready                   : out std_logic
        );
    end component;

    component memory is
    	generic(
            ENABLE_16_BIT					: integer range 0 to 1; -- Default: 0 = disabled, 1 = enabled
            -- Size of FIFO buffers
            FIFO_DEPTH_WRITE				: integer := 8; -- Default: 8
            FIFO_DEPTH_READ  				: integer := 8  -- Default: 8	
	    );
        port (
            clk_200MHz      				: in  std_logic; -- 200 MHz system clock => 5 ns period time
            rst             				: in  std_logic; -- active high system reset
            address 	     				: in  std_logic_vector(26 downto 0); -- address space
            data_in          				: in  std_logic_vector((8 * (1 + ENABLE_16_BIT) - 1) downto 0); -- data byte input
            r_w			     				: in  std_logic; -- Read or Write flag: '1' ... write, '0' ... read
            mem_ready						: out std_logic; -- allocated memory ready or busy flag: '1' ... ready, '0' ... busy
            data_out         				: out std_logic_vector((8 * (1 + ENABLE_16_BIT) - 1) downto 0); -- data byte output
            -- DDR2 interface
            ddr2_addr            : out   std_logic_vector(12 downto 0);
            ddr2_ba              : out   std_logic_vector(2 downto 0);
            ddr2_ras_n           : out   std_logic;
            ddr2_cas_n           : out   std_logic;
            ddr2_we_n            : out   std_logic;
            ddr2_ck_p            : out   std_logic_vector(0 downto 0);
            ddr2_ck_n            : out   std_logic_vector(0 downto 0);
            ddr2_cke             : out   std_logic_vector(0 downto 0);
            ddr2_cs_n            : out   std_logic_vector(0 downto 0);
            ddr2_dm              : out   std_logic_vector(1 downto 0);
            ddr2_odt             : out   std_logic_vector(0 downto 0);
            ddr2_dq              : inout std_logic_vector(15 downto 0);
            ddr2_dqs_p           : inout std_logic_vector(1 downto 0);
            ddr2_dqs_n           : inout std_logic_vector(1 downto 0)
        );
    end component;

begin
--
-------------------------------------------------------------------------------
-- Component Instantiation
-------------------------------------------------------------------------------
--
    -- team 5
    team5_inst: team5 
        generic map (
            DEG_OF_PARALLELISM    =>  DEG_OF_PARALLELISM,
            MAX_PASSWORD_LEN_BYTE =>  MAX_PASSWORD_LEN_BYTE,
            MAX_SALT_LEN_BYTE     =>  MAX_SALT_LEN_BYTE,
            MAX_TAG_LEN_BYTE      =>  MAX_TAG_LEN_BYTE
        )
        port map (
            reset                   => reset,
            clk                     => clk, 
            start                   => s_team5_start,
            tag_len_byte            => s_tag_len_byte_buf,
            password                => s_password_buf,
            password_len_byte       => s_password_len_byte_buf,
            salt                    => s_salt_buf,
            salt_len_byte           => s_salt_len_byte_buf,
            mem_r_w                 => s_mem_r_w_team5,
            mem_ready               => s_mem_ready_team5,
            mem_data_out            => s_mem_data_out_team5,
            mem_address             => s_mem_address_team5,
            mem_data_in             => s_mem_data_in_team5,
            block_rows_count        => s_block_rows_count,
            block_columns_count     => s_block_columns_count,
            ready                   => s_team5_ready
        );

    -- team 6
    team6_inst: team6
        generic map (
            DEG_OF_PARALLELISM  => DEG_OF_PARALLELISM,
            MAX_TAG_LEN_BYTE    => MAX_TAG_LEN_BYTE,
            MAX_ITERATION_COUNT => MAX_ITERATION_COUNT
        )
        port map (
            reset                   => reset,
            clk                     => clk, 
            start                   => s_team6_start,
            argon_type              => s_argon_type_buf,
            tag_len_byte            => s_tag_len_byte_buf,
            block_rows_count        => s_block_rows_count,
            block_columns_count     => s_block_columns_count,
            iteration_count         => s_iteration_count_buf,
            mem_r_w                 => s_mem_r_w_team6,
            mem_ready               => s_mem_ready_team6,
            mem_data_out            => s_mem_data_out_team6,
            mem_address             => s_mem_address_team6,
            mem_data_in             => s_mem_data_in_team6,
            current_iteration_count => current_iteration_count,
            tag                     => tag,
            ready                   => s_team6_ready
        );

        -- team 7 - Memory
        memory_inst: memory
            generic map (
                0, 8, 8
            )
            port map (
                clk_200MHz      => clk_200mhz,     -- here: 200MHz_signal required
                rst             => reset,
                address         => s_mem_address,
                data_in         => s_mem_data_in,
                r_w             => s_mem_r_w,
                mem_ready       => s_mem_ready,
                data_out        => s_mem_data_out,
                -- DDR2 interface
                ddr2_addr       => ddr2_addr,
                ddr2_ba         => ddr2_ba,
                ddr2_ras_n      => ddr2_ras_n,
                ddr2_cas_n      => ddr2_cas_n,
                ddr2_we_n       => ddr2_we_n,
                ddr2_ck_p       => ddr2_ck_p,
                ddr2_ck_n       => ddr2_ck_n,
                ddr2_cke        => ddr2_cke,
                ddr2_cs_n       => ddr2_cs_n,
                ddr2_dm         => ddr2_dm,
                ddr2_odt        => ddr2_odt,
                ddr2_dq         => ddr2_dq,
                ddr2_dqs_p      => ddr2_dqs_p,
                ddr2_dqs_n      => ddr2_dqs_n
            );
--
-------------------------------------------------------------------------------
--
-- ready signal pos edge detection
--
    edge : process (clk, reset)
    begin
        if(reset = '1') then
            s_team5_ready_pos_edge <= '0';
            s_team6_ready_pos_edge <= '0';

            s_team5_ready_ff <= "11";
            s_team6_ready_ff <= "11";

        elsif(clk'event and clk = '1') then
            s_team5_ready_pos_edge <= '0';
            s_team6_ready_pos_edge <= '0';

            s_team5_ready_ff <= s_team5_ready_ff(0) & s_team5_ready;
            s_team6_ready_ff <= s_team6_ready_ff(0) & s_team6_ready;

            if(s_team5_ready_ff(0) = '1' and s_team5_ready_ff(1) = '0') then
                s_team5_ready_pos_edge <= '1';
            end if;

            if(s_team6_ready_ff(0) = '1' and s_team6_ready_ff(1) = '0') then
                s_team6_ready_pos_edge <= '1';
            end if;
        end if;

    end process;
--
-------------------------------------------------------------------------------
--
-- state machine for component activation
--
    sync : process (clk, reset)
    begin
        if(reset = '1') then
            state         <= STATE_IDLE;
            s_ready       <= '0';
            s_team5_start <= '0';
            s_team6_start <= '0';

        elsif(CLK'event and CLK='1') then   
            state         <= state_next;
            s_ready       <= s_ready_next;
            s_team5_start <= s_team5_start_next;
            s_team6_start <= s_team6_start_next;
        end if;
    end process sync;

    state_machine : process (clk)
    begin
        state_next         <= state;
        s_ready_next       <= s_ready;
        s_team5_start_next <= s_team5_start;
        s_team6_start_next <= s_team6_start;

        case state is
            when STATE_IDLE => 
                s_ready_next <= '1';
                
                if(start = '1' and s_team5_ready = '1' and s_team6_ready = '1') then
                    s_password_buf          <= password;
                    s_password_len_byte_buf <= password_len_byte;
                    s_iteration_count_buf   <= iteration_count;
                    s_tag_len_byte_buf      <= tag_len_byte;
                    s_argon_type_buf        <= argon_type;
                    s_salt_buf              <= salt;
                    s_salt_len_byte_buf     <= salt_len_byte;

                    s_ready_next       <= '0';
                    s_team5_start_next <= '1';
                    state_next         <= STATE_TEAM5;
                end if;

            when STATE_TEAM5 =>
                --if team5 finishes it calculations 
                if(s_team5_ready_pos_edge = '1') then
                    --team 6 starts processing
                    s_team6_start_next <= '1';
                    state_next         <= STATE_TEAM6;
                end if;

                s_team5_start_next   <= '0';

                -- map memory component to team 5
                s_mem_address        <= s_mem_address_team5;
                s_mem_data_in        <= s_mem_data_in_team5;
                s_mem_data_out_team5 <= s_mem_data_out;
                s_mem_r_w_team5      <= s_mem_r_w;
                s_mem_ready_team5    <= s_mem_ready;

            when STATE_TEAM6 =>
                if(s_team6_ready_pos_edge = '1') then
                    s_ready_next <= '1';
                    state_next   <= STATE_IDLE;
                end if;
                
                s_team6_start_next <= '0';

                -- map memory conponent to team 6
                s_mem_address        <= s_mem_address_team6;
                s_mem_data_in        <= s_mem_data_in_team6;
                s_mem_data_out_team6 <= s_mem_data_out;
                s_mem_r_w_team6      <= s_mem_r_w;
                s_mem_ready_team6    <= s_mem_ready;
            end case;
    end process state_machine;
--
-------------------------------------------------------------------------------
--
-- map signals to outputs
--
    ready <= s_ready;
end behavioral;