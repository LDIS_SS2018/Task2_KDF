#------------------------------------------------------------------------------
#
# Synthesis script using Digilent Nexys 4 DDR board
#
# -----------------------------------------------------------------------------
#
create_project -part xc7a100t -force [lindex $argv 0]
#
# -----------------------------------------------------------------------------
#
read_vhdl team5.vhd
read_vhdl team6.vhd

read_vhdl ./06/compress/compress.vhd
read_vhdl ./06/permutate/permutate.vhd
read_vhdl ./06/trunc/trunc.vhd

read_vhdl ./07/Memory.vhd
read_vhdl ./07/Ram2Ddr_RefComp/Source/Ram2DdrXadc_RefComp/ram2ddrxadc_pkg.vhd
read_vhdl ./07/Ram2Ddr_RefComp/Source/Ram2DdrXadc_RefComp/ram2ddrxadc.vhd
read_vhdl ./07/fifo_pkg.vhd
read_vhdl ./07/fifo.vhd

read_vhdl ./15_blake2b/task-2/design.vhd

read_vhdl [lindex $argv 0].vhd
read_xdc  [lindex $argv 0].xdc
#
# -----------------------------------------------------------------------------
#
synth_design -top [lindex $argv 0]
#
# -----------------------------------------------------------------------------
#
opt_design
place_design
route_design
#
# -----------------------------------------------------------------------------
#
write_bitstream -force [lindex $argv 0].bit
#
# -----------------------------------------------------------------------------
